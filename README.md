
# Docker Compose, WordPress, MariaDb, phpmyadmin, PHP Composer, Sage, phpmyadmin
 
## Folder structure

![Alt text](Folder Structure.png "Folder Structure")


## Run

```shell
docker-compose up -d
```

## Install & create Sage project
[Sage Docs](https://roots.io/sage/docs/theme-installation/)

 
```shell
docker-compose run composer create-project roots/sage project-name
```

# TODO 

Add nodejs & sage npm install process to the docker compose process.

* Right now you must navigate in your local folder to `/src/wp-content/themes/{project-name}` and proceed to install the npm dependencies of sage manually.


🚀 Open up [http://localhost:8000/](http://localhost:8000/)


### Notes:

When making changes to the Dockerfile, use:

```bash
docker-compose up -d --force-recreate --build
```
 

### Useful Docker Commands

Login to the docker container

```shell
docker exec -it myapp-wordpress bash
```

Stop

```shell
docker-compose stop
```

Down (stop and remove)

```shell
docker-compose down
```

Cleanup

```shell
docker-compose rm -v
```

Recreate

```shell
docker-compose up -d --force-recreate
```

Rebuild docker container when Dockerfile has changed

```shell
docker-compose up -d --force-recreate --build
```
 
